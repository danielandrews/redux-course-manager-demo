import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './App.css';
import Header from './common/Header';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from 'react-router-dom';

class App extends Component {

  static propTypes = {
    loading: PropTypes.bool.isRequired,
    children: PropTypes.object.isRequired
  };

  render() {
    return (
      <div id="app">
        <Header loading={this.props.loading} />
        <hr />
        {this.props.children}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    loading: state.numAjaxCallsInProgress > 0
  }
}

export default compose(
  withRouter,
  connect(mapStateToProps)
)(App);
