import React from 'react';
import PropTypes from 'prop-types';

class LoadingDots extends React.Component {

    static defaultProps = {
        interval: 300, dots: 3
    };
    
    static propTypes = {
        interval: PropTypes.number.isRequired,
        dots: PropTypes.number.isRequired
    };

    state = { frame: 1 };

    componentDidMount() {
        this.interval = setInterval(() => {
            this.setState({
                frame: this.state.frame + 1 // eslint-disable-line react/no-did-mount-set-state
            });
        }, this.props.interval);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        let dots = this.state.frame % (this.props.dots + 1);
        let text = '';
        while (dots > 0) {
            text += '.';
            dots--;
        }
        return (<span {...this.props}>{text}&nbsp;</span>);
    }
}

export default LoadingDots;