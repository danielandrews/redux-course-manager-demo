import React from 'react';

class AboutPage extends React.Component {
  render() {
    return (
      <div>
        <h1>About</h1>
        <p>This app uses React, Redux, and React Router to demo a simple course management system</p>
      </div>
    );
  }
}

export default AboutPage;
