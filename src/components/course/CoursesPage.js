import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import PropTypes from 'prop-types';
import * as courseActions from '../../actions/courseActions';
import CourseList from './CourseList';
import { withRouter } from 'react-router-dom';

class CoursePage extends React.Component {

    static propTypes = {
        courses: PropTypes.array.isRequired,
        actions: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired
    };

    courseRow = ({ title }, index) => {
        return (
            <div key={index}>{title}</div>
        );
    }

    redirectToCoursePage = () => {
        this.props.history.push('/course');
    };

    render() {
        const { courses } = this.props;
        return (
            <div>
                <h1>Courses</h1>
                <input
                    type="submit"
                    value="Add Course"
                    className="btn btn-primary"
                    onClick={this.redirectToCoursePage}
                />
                <CourseList courses={courses} />
            </div>
        );
    }
}

const mapStateToProps = ({ courses }) => { return { courses } };
const mapDispatchToProps = (dispatch) => { return { actions: bindActionCreators(courseActions, dispatch) } };

export default compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps)
)(CoursePage);