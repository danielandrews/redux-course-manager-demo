import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import PropTypes from 'prop-types';
import * as courseActions from '../../actions/courseActions';
import CourseForm from './CourseForm';
import { withRouter } from 'react-router-dom';
import toastr from 'toastr';
class ManageCoursePage extends React.Component {

    static propTypes = {
        course: PropTypes.object.isRequired,
        authors: PropTypes.array.isRequired,
        actions: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired
    };

    state = {
        course: { ...this.props.course },
        errors: {},
        saving: false
    };

    componentWillReceiveProps(nextProps) {
        if (this.props.course.id !== nextProps.course.id) {
            this.setState({
                course: { ...nextProps.course }
            });
        }
    }

    updateCourseState = ({ target }) => {
        const field = target.name;
        let course = { ...this.state.course };
        course[field] = target.value;
        this.setState({ course });
    };

    saveCourse = async (event) => {
        event.preventDefault();
        this.setState({ saving: true });

        try {
            const savedCourse = await this.props.actions.saveCourse(this.state.course);
            if (savedCourse) {
                this.redirect();
            }
        }
        catch (error) {
            toastr.error(error);
            this.setState({ saving: false });
        }
    };

    redirect = () => {
        this.setState({ saving: true });
        toastr.success('Course Saved');
        this.props.history.push('/courses');
    }

    render() {
        const { course, errors } = this.state;
        const { authors } = this.props;

        return (
            <CourseForm
                allAuthors={authors}
                course={course}
                errors={errors}
                saving={this.state.saving}
                onChange={this.updateCourseState}
                onSave={this.saveCourse}
            />
        );
    }
}

function getCourseById(courses, id) {
    return courses.filter(course => course.id === id)[0];
}

const mapStateToProps = (state, ownProps) => {
    let course = {
        id: '',
        watchHref: '',
        title: '',
        authorId: '',
        length: '',
        category: ''
    }

    const courseId = ownProps.match.params.id;

    if (courseId && state.courses.length > 0) {
        course = getCourseById(state.courses, courseId);
    }

    const authorsFormattedForDropdown = state.authors.map(author => {
        return {
            value: author.id,
            text: author.firstName + ' ' + author.lastName
        };
    });

    return { course, authors: authorsFormattedForDropdown };
};

const mapDispatchToProps = (dispatch) => { return { actions: bindActionCreators(courseActions, dispatch) } };

export default compose(
    withRouter,
    connect(mapStateToProps, mapDispatchToProps)
)(ManageCoursePage);