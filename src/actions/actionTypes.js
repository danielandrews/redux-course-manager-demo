import keyMirror from 'keymirror';

const actionTypes = keyMirror({
    LOAD_COURSES_SUCCESS: null,
    LOAD_AUTHORS_SUCCESS: null,
    CREATE_COURSE_SUCCESS: null,
    UPDATE_COURSE_SUCCESS: null,
    BEGIN_AJAX_CALL: null,
    AJAX_CALL_ERROR: null
});

export default actionTypes;