import actionTypes from './actionTypes';
import authorApi from '../api/mockAuthorApi';
import { beginAjaxCall } from './ajaxStatusActions';

export function loadAuthorsSuccess(authors) {
    return { type: actionTypes.LOAD_AUTHORS_SUCCESS, authors }
}

export function loadAuthors() {
    return async (dispatch) => {
        dispatch(beginAjaxCall());
        try {
            const authors = await authorApi.getAllAuthors();
            dispatch(loadAuthorsSuccess(authors));
        } catch (error) {
            throw (error);
        }
    }
}