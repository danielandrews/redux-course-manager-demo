import actionTypes from './actionTypes';
import courseApi from '../api/mockCourseApi';
import { beginAjaxCall, ajaxCallError } from './ajaxStatusActions';

export function loadCoursesSuccess(courses) {
    return { type: actionTypes.LOAD_COURSES_SUCCESS, courses }
}

export function createCourseSuccess(course) {
    return { type: actionTypes.CREATE_COURSE_SUCCESS, course }
}

export function updateCourseSuccess(course) {
    return { type: actionTypes.UPDATE_COURSE_SUCCESS, course }
}

export function loadCourses() {
    return async (dispatch) => {
        dispatch(beginAjaxCall());
        try {
            const courses = await courseApi.getAllCourses();
            dispatch(loadCoursesSuccess(courses));
        } catch (error) {
            throw (error);
        }
    }
}

export function saveCourse(course) {
    return async (dispatch, getState) => {
        dispatch(beginAjaxCall());
        try {
            const savedCourse = await courseApi.saveCourse(course);
            course.id ? dispatch(updateCourseSuccess(savedCourse)) : dispatch(createCourseSuccess(savedCourse));
            return savedCourse;
        } catch (error) {
            dispatch(ajaxCallError(error));
            throw (error);
        }
    }
}